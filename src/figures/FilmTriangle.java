package figures;

import figures.exceptions.WrongDataException;
import materials.IFilm;

import java.io.Serializable;

/**
 * class Film Triangle which extends abstract class Triangle and implements interface IFilm
 * @author Nikolay Sizykh
 * @version 1.1
 */

public class FilmTriangle extends Triangle implements IFilm, Serializable {

    private static final long SerialVersionUID = 5l;

    public FilmTriangle(double side) throws WrongDataException {
        super(side);
    }

    public FilmTriangle(Figure figure, double difArea) throws WrongDataException {
        super(figure, difArea);
    }

    public FilmTriangle() {
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return "FilmTriangle{" +
                "side=" + side +
                ", area = " + getArea() +
                ", perimeter = " + getPerimeter() +

                '}' + "\n";
    }
}
