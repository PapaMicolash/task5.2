package figures;

import figures.exceptions.WrongDataException;
import materials.IFilm;

import java.io.Serializable;

/**
 * class Film triangle which extends abstract class Triangle and implements interface IFilm
 * @author Nikolay Sizykh
 * @version 1.1
 */

public class FilmCircle extends Circle implements IFilm, Serializable {

    private static final long SerialVersionUID = 3l;

    public FilmCircle(double radius) throws WrongDataException {
        super(radius);
    }

    public FilmCircle(Figure figure, double difArea) throws WrongDataException {
        super(figure, difArea);
    }

    public FilmCircle() {
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return "FilmCircle{" +
                "radius = " + radius +
                ", area = " + getArea() +
                ", perimeter = " + getPerimeter() +
                '}' + "\n";
    }
}
