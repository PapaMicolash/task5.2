package figures;

import figures.exceptions.WrongDataException;
import materials.Color;
import materials.IPaper;

import java.io.Serializable;
import java.util.Objects;

/**
 * class Paper Circle which extends abstract class Rectangle and implements interface IPaper
 * @author Nikolay Sizykh
 * @version 1.1
 */

public class PaperCircle extends Circle implements IPaper, Serializable {

    private static final long SerialVersionUID = 6l;

    Color color = null;

    public PaperCircle(double radius, Color color) throws WrongDataException {
        super(radius);
        this.color = color;
    }

    public PaperCircle(double radius) throws WrongDataException {
        super(radius);
    }

    public PaperCircle(Figure figure, double difArea)  throws WrongDataException {
        super(figure, difArea);
        if (figure instanceof IPaper) {
            this.color = ((IPaper) figure).getColor();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PaperCircle that = (PaperCircle) o;
        return color == that.color;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), color);
    }

    /**
     * this is boolean method which check and paint figure if figure without color or dont paint if figure with color
     * @param color this is a part of enum class Color
     * @return true or false
     */
    @Override
    public boolean paint(Color color) {
        if (this.color == null) {
            this.color = color;
            return true;
        }
        else return false;
    }

    @Override
    public Color getColor() {
        return color;
    }

    @Override
    public String toString() {
        return "PaperCircle{" +
                "color = " + color +
                ", radius = " + radius +
                ", area = " + getArea() +
                ", perimeter = " + getPerimeter() +

                '}' + "\n";
    }
}
