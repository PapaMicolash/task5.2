package figures;

import comparators.ColorComparator;
import comparators.ShapeComparator;
import comparators.SquareComparator;
import figures.exceptions.WrongDataException;
import materials.IFilm;
import materials.IPaper;

import java.io.Serializable;
import java.util.ArrayList;

public class BoxFigures implements Serializable {
    private static BoxFigures ourInstance = new BoxFigures();
    private static final long SerialVersionUID = 1l;

    ArrayList<Figure> figures = new ArrayList<>();

    public static BoxFigures getOurInstance() {
        return ourInstance;
    }

    public static void setOurInstance(BoxFigures ourInstance) {
        BoxFigures.ourInstance = ourInstance;
    }

    public ArrayList<Figure> getFigures() {
        return figures;
    }

    public void setFigures(ArrayList<Figure> figures) {
        this.figures = figures;
    }

    public static BoxFigures getInstance() {
        return ourInstance;
    }

    private BoxFigures() {
    }

    public void createBox(Figure figure) {
        if (figures.size() < 1) {
            figures.add(figure);
        }
    }

    public void addFigures(Figure figure) {
        if (checkBox(figure)) {
            figures.add(figure);
        }
    }

    public boolean checkBox(Figure figure) {
        boolean check = false;

        if (figures.size() < 20) {
            for (int i = 0; i < figures.size(); i++) {

                if (!figures.get(i).equals(figure)) {
                    check = true;
                } else {
                    check = false;
                }
            }
        }

        return check;
    }

    /**
     *    !!!!!!!!TASK 5.2 !!!!!!!
     */
    public ArrayList<Figure> getFiguresFromGroup(Class c) {
        ArrayList<Figure> classFigures = new ArrayList<>();

        for (int i = 0; i < figures.size(); i++) {
            if (c.isInstance(figures.get(i))) {
                classFigures.add(figures.get(i));
            }
        }

        return classFigures;
    }

    public void sortBoxByColor() {
        figures.sort(new ColorComparator());
    }

    public void sortBoxByShape() {
        figures.sort(new ShapeComparator());
    }

    public void sortBoxByArea() {
        figures.sort(new SquareComparator());
    }

    @Override
    public String toString() {
        return "BoxFigures{" +
                "figures=" + figures +
                '}';
    }
}
