package figures;

import figures.exceptions.WrongDataException;
import materials.Color;
import materials.IPaper;

import java.io.Serializable;
import java.util.Objects;

/**
 * class Paper Rectangle which extends abstract class Rectangle and implements interface IPaper
 * @author Nikolay Sizykh
 * @version 1.1
 */

public class PaperRectangle extends Rectangle implements IPaper, Serializable {

    private static final long SerialVersionUID = 7l;

    Color color = null;

    public PaperRectangle(double sideA, double sideB, Color color) throws WrongDataException {
        super(sideA, sideB);
        this.color = color;
    }

    public PaperRectangle(double sideA, double sideB) throws WrongDataException {
        super(sideA, sideB);
    }

    public PaperRectangle(Figure figure, double difArea) {
        super(figure, difArea);
        if (figure instanceof IPaper) {
            this.color = ((IPaper) figure).getColor();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PaperRectangle that = (PaperRectangle) o;
        return color == that.color;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), color);
    }

    /**
     * this is boolean method which check and paint figure if figure without color or dont paint if figure with color
     * @param color this is a part of enum class Color
     * @return true or false
     */
    @Override
    public boolean paint(Color color) {
        if (this.color == null) {
            this.color = color;
            return true;
        }
        else return false;
    }

    @Override
    public Color getColor() {
        return color;
    }

    @Override
    public String toString() {
        return "PaperRectangle{" +
                "color=" + color +
                ", sideA=" + sideA +
                ", sideB=" + sideB +
                ", area = " + getArea() +
                ", perimeter = " + getPerimeter() +

                '}' + "\n";
    }
}
