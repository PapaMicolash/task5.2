package figures;

import figures.exceptions.WrongDataException;
import materials.IFilm;

import java.io.Serializable;

/**
 * class Film Rectangle which extends abstract class Rectangle and implements interface IFilm
 * @author Nikolay Sizykh
 * @version 1.1
 */

public class FilmRectangle extends Rectangle implements IFilm, Serializable {

    private static final long SerialVersionUID = 4l;

    public FilmRectangle(double sideA, double sideB) throws WrongDataException {
        super(sideA, sideB);
    }

    public FilmRectangle(Figure figure, double difArea) throws WrongDataException {
        super(figure, difArea);
    }

    public FilmRectangle() {
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return "FilmRectangle{" +
                "sideA = " + sideA +
                ", sideB = " + sideB +
                ", area = " + getArea() +
                ", perimeter = " + getPerimeter() +
                '}' + "\n";
    }
}
