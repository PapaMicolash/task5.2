package figures;

import figures.exceptions.WrongDataException;

import java.io.Serializable;
import java.util.Objects;

/**
 * Abstract class for Triangle
 * @author Nikolay Sizykh
 * @version 1.1
 */

public abstract class Triangle extends Figure  implements Serializable {

    private static final long SerialVersionUID = 10;

    /**
     * a side of triangle
     */
    double side;

    public Triangle(double side) throws WrongDataException {
        if (side < 0) {
            throw new WrongDataException("side of triangle less than 0");
        }
        this.side = side;
    }

    public Triangle() {
    }

    /**
     * this constructor for cutting triangles
     * @param figure a figure for cutting
     * @param difArea a difference between areas of existing figure and new figure
     */
    public Triangle(Figure figure, double difArea) {
        this.side = Math.sqrt((4 * (figure.getArea()) - difArea) / Math.sqrt(3));
    }



    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    @Override
    public double getArea() {
        return (Math.sqrt(3) / 4) * Math.pow(side, 2);
    }

    @Override
    public double getPerimeter() {
        return side * 3;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Triangle)) return false;
        if (!super.equals(o)) return false;
        Triangle triangle = (Triangle) o;
        return Double.compare(triangle.side, side) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), side);
    }

    @Override
    public String toString() {
        return "Triangle{" +
                "side=" + side +
                ", area = " + getArea() +
                ", perimeter = " + getPerimeter() +
                '}';
    }
}
