import figures.*;
import figures.exceptions.SerializaitionException;
import figures.exceptions.WrongDataException;
import materials.Color;

import java.awt.print.Paper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.SortedMap;

/**
 * class Main serves for creation figures and displays any information for task
 * @author Nikolay Sizykh
 * @version 1.1
 */

public class Main {



    public static void main(String[] args) throws IOException, ClassNotFoundException, WrongDataException {

        BoxFigures boxFigures = null;


        /*PaperRectangle yellowRectangle = new PaperRectangle(1.2, 1.3);
        yellowRectangle.paint(Color.YELLOW);
        PaperRectangle redRectangle = new PaperRectangle(1.1, 1.5);
        redRectangle.paint(Color.RED);
        PaperCircle blueCircle = new PaperCircle(1.5);
        blueCircle.paint(Color.BLUE);
        PaperCircle blackCircle = new PaperCircle(2.1);
        blackCircle.paint(Color.BLACK);
        PaperTriangle whiteTriangle = new PaperTriangle(1.6);
        whiteTriangle.paint(Color.WHITE);
        PaperTriangle purpleTriangle = new PaperTriangle(1.3);
        purpleTriangle.paint(Color.PURPLE);
        PaperTriangle azureTriangle = new PaperTriangle(0.9);
        azureTriangle.paint(Color.AZURE);
        FilmTriangle filmTriangle = new FilmTriangle(0.9);
        FilmCircle filmCircle = new FilmCircle(0.5);
        FilmRectangle filmRectangle = new FilmRectangle(0.7, 0.6);


        boxFigures = BoxFigures.getInstance();

        boxFigures.createBox(yellowRectangle);
        boxFigures.addFigures(redRectangle);
        boxFigures.addFigures(blueCircle);
        boxFigures.addFigures(blackCircle);
        boxFigures.addFigures(whiteTriangle);
        boxFigures.addFigures(purpleTriangle);
        boxFigures.addFigures(azureTriangle);
        boxFigures.addFigures(filmCircle);
        boxFigures.addFigures(filmRectangle);
        boxFigures.addFigures(filmTriangle);




        System.out.println(boxFigures.toString());


        try {
            new BinFileFigure().save2Bin();
        } catch (SerializaitionException e) {
            e.printStackTrace();
        }

        ArrayList<Figure> circleFigures = boxFigures.getFiguresFromGroup(Triangle.class);

        System.out.println(circleFigures.toString());

        boxFigures.sortBoxByColor();

        System.out.println(boxFigures.toString());

        boxFigures.sortBoxByShape();

        System.out.println(boxFigures.toString());

        boxFigures.sortBoxByArea();


        try {
            new BinFileFigure().save2Bin();


        } catch (SerializaitionException e) {
            e.printStackTrace();
        }*/



        try {
            new BinFileFigure().loadFromBin();
            boxFigures = BoxFigures.getInstance();
            ArrayList<Figure> figures = boxFigures.getFigures();
            System.out.println(figures.toString());
        } catch (SerializaitionException e) {
            e.printStackTrace();
        }

        PaperRectangle paperRectangle = new PaperRectangle(0.3, 0.3);
        paperRectangle.paint(Color.BLACK);
        boxFigures.addFigures(paperRectangle);

        ArrayList<Figure> circleFigures = boxFigures.getFiguresFromGroup(Triangle.class);

        System.out.println(circleFigures.toString());

        boxFigures.sortBoxByColor();

        System.out.println(boxFigures.toString());

        boxFigures.sortBoxByShape();

        System.out.println(boxFigures.toString());

        boxFigures.sortBoxByArea();

        System.out.println(boxFigures.toString());

    }
}
