package comparators;

import figures.Figure;

import java.util.Comparator;

public class SquareComparator implements Comparator<Figure> {
    @Override
    public int compare(Figure o1, Figure o2) {
        if (o1.getArea() > o2.getArea()) {
            return 1;
        } else if (o1.getArea() < o2.getArea()) {
            return -1;
        } else {
            return 0;
        }
    }
}
