package comparators;

import figures.Figure;

import java.util.Comparator;

public class ShapeComparator implements Comparator<Figure> {
    @Override
    public int compare(Figure o1, Figure o2) {
        String classO1 = o1.getClass().getSimpleName();
        String classO2 = o2.getClass().getSimpleName();

        return classO1.compareTo(classO2);
    }
}
