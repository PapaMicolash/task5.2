package comparators;

import figures.Figure;
import materials.IPaper;


import java.util.Comparator;

public class ColorComparator implements Comparator<Figure> {
    @Override
    public int compare(Figure o1, Figure o2) {
        String colorO1 = null;
        String colorO2 = null;

        if (o1 instanceof IPaper && ((IPaper) o1).getColor() != null) {
            colorO1 = ((IPaper) o1).getColor().toString();
        } else {
            colorO1 = "NONE";
        }

        if (o2 instanceof IPaper && ((IPaper) o2).getColor() != null) {
            colorO2 = ((IPaper) o2).getColor().toString();
        } else {
            colorO2 = "NONE";
        }

        return colorO1.compareTo(colorO2);
    }


}
